<?php

class AppointmentsController extends AppController {

	var $name = 'Appointments';
	var $helpers = array('Html', 'Form', 'Text', 'Time' );
    var $components = array("obAuth");

	function index() {
		$this->obAuth->lock();
		$this->Appointment->recursive = 0;
		$this->set('appointments', $this->Appointment->findAll());
	}

	function view($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for Appointment.');
			$this->redirect('/appointments/index');
		}
		vendor('ofc-library/open_flash_chart_object');
		$this->set('appointment', $this->Appointment->read(null, $id));
	}
	
	
	function graph($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for Appointment.');
			$this->redirect('/appointments/index');
		}
		$this->layout = 'raw';
		vendor('ofc-library/open-flash-chart');
		$appointment = $this->set('appointment',$this->Appointment->read(null, $id));
	}

	function add() {
		$this->obAuth->lock();
		if(empty($this->data)) {
			$this->set('students', $this->Appointment->Student->generateList(null, null, null, null, "{n}.Student.FirstName"));
			$this->set('users', $this->Appointment->User->generateList());
			$this->render();
		} else {
			$this->cleanUpFields();
			if($this->Appointment->save($this->data)) {
			$fileData = fread(fopen($this->params["form"]["recording1"]['tmp_name'], "r"),
                                     $this->params["form"]["recording1"]['size']);
									 //echo var_dump( $fileData );
									 //@TODO save this filedata*/
				//MyFlesController::MyFile->save($this->params["form"]["recording1"]);
				$this->Session->setFlash('The Appointment has been saved');
				$this->redirect('/files/add');
			} else {
				$this->Session->setFlash('Please correct errors below.');
				$this->set('students', $this->Appointment->Student->generateList());
				$this->set('users', $this->Appointment->User->generateList());
			}
		}
	}

	function edit($id = null) {
		$this->obAuth->lock();
		if(empty($this->data)) {
			if(!$id) {
				$this->Session->setFlash('Invalid id for Appointment');
				$this->redirect('/appointments/index');
			}
			$this->data = $this->Appointment->read(null, $id);
			$this->set('students', $this->Appointment->Student->generateList());
			$this->set('users', $this->Appointment->User->generateList());
		} else {
			$this->cleanUpFields();
			if($this->Appointment->save($this->data)) {
				$this->Session->setFlash('The Appointment has been saved');
				$this->redirect('/appointments/index');
			} else {
				$this->Session->setFlash('Please correct errors below.');
				$this->set('students', $this->Appointment->Student->generateList());
				$this->set('users', $this->Appointment->User->generateList());
			}
		}
	}

	function delete($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for Appointment');
			$this->redirect('/appointments/index');
		}
		if($this->Appointment->del($id)) {
			$this->Session->setFlash('The Appointment deleted: id '.$id.'');
			//$this->Session->setFlash('The Appointment deleted: '$student['Student']['FirstName'] ." ". $student['Student']['LastName'].);
			$this->redirect('/appointments/index');
		}
	}

}
?>
