<?php
class FilesController extends AppController
{
	var $components = array("obAuth");
	
    function add()
    {
        if (!empty($this->params['form']) &&
             is_uploaded_file($this->params['form']['File']['tmp_name']))
        {
            $fileData = fread(fopen($this->params['form']['File']['tmp_name'], "r"),
                                     $this->params['form']['File']['size']);
            $this->params['form']['File']['data'] = $fileData;

            $this->File->save($this->params['form']['File']);

            $this->redirect('appointments/index');
        }
    }

function download($id)
{
	$this->obAuth->lock();
    $file = $this->File->findById($id);

    header('Content-type: ' . $file['File']['type']);
    header('Content-length: ' . $file['File']['size']);
    header('Content-Disposition: attachment; filename='.$file['File']['name']);
    echo $file['File']['data'];

    exit();
}

}
?>
