<?php

class StudentsController extends AppController {

	var $name = 'Students';
	var $uses = array('Student', 'Guardian', 'Relationship', 'Appointment', 'User');
	var $helpers = array('Html', 'Form', 'Text', 'Time' );
	var $components = array('obAuth');

	function index() {
		$this->obAuth->lock();
		$this->Student->recursive = 0;
  		$this->set('students', $this->Student->findAll( "id IN (SELECT student_id FROM appointments WHERE user_id = ".$this->obAuth->getUserId().") OR id NOT IN (SELECT student_id FROM appointments)" , array('FirstName','LastName','Grade', 'DOB') ) ); 
		$this->set('appointments', $this->Appointment->findAll());
	}

	function view($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for Student.');
			$this->redirect('/students/index');
		}
		$this->set('student', $this->Student->read(null, $id));
	}

	function add() {
		$this->obAuth->lock();
		if(empty($this->data)) {
			$this->set('guardians', $this->Student->Guardian->generateList());
			$this->set('selectedGuardians', null);
			$this->render();
		} else {
			$this->cleanUpFields();
			if($this->Student->save($this->data)) {
				$this->Session->setFlash('The Student has been saved');
				$this->redirect('/students/index');
			} else {
				$this->Session->setFlash('Please correct errors below.');
				$this->set('guardians', $this->Student->Guardian->generateList());
				if(empty($this->data['Guardian']['Guardian'])) { $this->data['Guardian']['Guardian'] = null; }
				$this->set('selectedGuardians', $this->data['Guardian']['Guardian']);
			}
		}
	}

	function edit($id = null) {
		$this->obAuth->lock();
		if(empty($this->data)) {
			if(!$id) {
				$this->Session->setFlash('Invalid id for Student');
				$this->redirect('/students/index');
			}
			$this->data = $this->Student->read(null, $id);
			$this->set('guardians', $this->Student->Guardian->generateList());
			if(empty($this->data['Guardian'])) { $this->data['Guardian'] = null; }
			$this->set('selectedGuardians', $this->_selectedArray($this->data['Guardian']));
		} else {
			$this->cleanUpFields();
			if($this->Student->save($this->data)) {
				$this->Session->setFlash('The Student has been saved');
				$this->redirect('/students/index');
			} else {
				$this->Session->setFlash('Please correct errors below.');
				$this->set('guardians', $this->Student->Guardian->generateList());
				if(empty($this->data['Guardian']['Guardian'])) { $this->data['Guardian']['Guardian'] = null; }
				$this->set('selectedGuardians', $this->data['Guardian']['Guardian']);
			}
		}
	}

	function delete($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for Student');
			$this->redirect('/students/index');
		}
		if($this->Student->del($id)) {
			$this->Session->setFlash('The Student deleted: id '.$id.'');
			$this->redirect('/students/index');
		}
	}

}
?>
