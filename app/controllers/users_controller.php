<?php
class UsersController extends AppController {

	var $name = 'Users';
	var $helpers = array('Html', 'Form' );
    var $components = array("obAuth");

	function index() {
		$this->obAuth->lock();
		$this->User->recursive = 0;
		$this->set('users', $this->User->findAll());
	}

	function view($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for User.');
			$this->redirect('/users/index');
		}
		$this->set('user', $this->User->read(null, $id));
	}

	function add() {
		$this->obAuth->lock();
		if(empty($this->data)) {
			$this->render();
		} else {
			$this->cleanUpFields();
			if($this->User->save($this->data)) {
				$this->Session->setFlash('The User has been saved');
				$this->redirect('/users/index');
			} else {
				$this->Session->setFlash('Please correct errors below.');
			}
		}
	}

	function edit($id = null) {
		$this->obAuth->lock();
		if(empty($this->data)) {
			if(!$id) {
				$this->Session->setFlash('Invalid id for User');
				$this->redirect('/users/index');
			}
			$this->data = $this->User->read(null, $id);
		} else {
			$this->cleanUpFields();
			if($this->User->save($this->data)) {
				$this->Session->setFlash('The User has been saved');
				$this->redirect('/users/index');
			} else {
				$this->Session->setFlash('Please correct errors below.');
			}
		}
	}

	function delete($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for User');
			$this->redirect('/users/index');
		}
		if($this->User->del($id)) {
			$this->Session->setFlash('The User deleted: id '.$id.'');
			$this->redirect('/users/index');
		}
	}
	function login(){
    	if(isset($this->data['User'])){
        	if($this->obAuth->login($this->data['User'])){
            	$this->redirect('/students');
			}else{
        	   $this->flash('Username/Password is incorrect', '/', '2');
            }
		}
	}

	function logout(){
    	$this->obAuth->lock();
    	$this->obAuth->logout();
    	$this->flash('You are now logged out.');
    	$this->redirect('/');
	}

}
?>
