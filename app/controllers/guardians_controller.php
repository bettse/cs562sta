<?php
class GuardiansController extends AppController {

	var $name = 'Guardians';
	var $helpers = array('Html', 'Form' );
    var $components = array("obAuth");

	function index() {
		$this->obAuth->lock();
		$this->Guardian->recursive = 0;
		$this->set('guardians', $this->Guardian->findAll());
	}

	function view($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for Guardian.');
			$this->redirect('/guardians/index');
		}
		$this->set('guardian', $this->Guardian->read(null, $id));
	}

	function add() {
		$this->obAuth->lock();
		if(empty($this->data)) {
			$this->set('students', $this->Guardian->Student->generateList());
			$this->set('selectedStudents', null);
			$this->render();
		} else {
			$this->cleanUpFields();
			if($this->Guardian->save($this->data)) {
				$this->Session->setFlash('The Guardian has been saved');
				$this->redirect('/guardians/index');
			} else {
				$this->Session->setFlash('Please correct errors below.');
				$this->set('students', $this->Guardian->Student->generateList());
				if(empty($this->data['Student']['Student'])) { $this->data['Student']['Student'] = null; }
				$this->set('selectedStudents', $this->data['Student']['Student']);
			}
		}
	}

	function edit($id = null) {
		$this->obAuth->lock();
		if(empty($this->data)) {
			if(!$id) {
				$this->Session->setFlash('Invalid id for Guardian');
				$this->redirect('/guardians/index');
			}
			$this->data = $this->Guardian->read(null, $id);
			$this->set('students', $this->Guardian->Student->generateList());
			if(empty($this->data['Student'])) { $this->data['Student'] = null; }
			$this->set('selectedStudents', $this->_selectedArray($this->data['Student']));
		} else {
			$this->cleanUpFields();
			if($this->Guardian->save($this->data)) {
				$this->Session->setFlash('The Guardian has been saved');
				$this->redirect('/guardians/index');
			} else {
				$this->Session->setFlash('Please correct errors below.');
				$this->set('students', $this->Guardian->Student->generateList());
				if(empty($this->data['Student']['Student'])) { $this->data['Student']['Student'] = null; }
				$this->set('selectedStudents', $this->data['Student']['Student']);
			}
		}
	}

	function delete($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for Guardian');
			$this->redirect('/guardians/index');
		}
		if($this->Guardian->del($id)) {
			$this->Session->setFlash('The Guardian deleted: id '.$id.'');
			$this->redirect('/guardians/index');
		}
	}

}
?>
