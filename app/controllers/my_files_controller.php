<?php
class MyFilesController extends AppController {
    var $components = array("obAuth");


    function add() {
        if (!empty($this->params['form']) &&
             is_uploaded_file($this->params['form']['File']['tmp_name'])) {
            $fileData = fread(fopen($this->params['form']['File']['tmp_name'], "r"),
                                     $this->params['form']['File']['size']);
            $this->params['form']['File']['data'] = $fileData;

            $this->MyFile->save($this->params['form']['File']);

            $this->redirect('students/index');
        }
    }

	function download($id) {
        $this->obAuth->lock();
    	Configure::write('debug', 0);
    	$file = $this->MyFile->findById($id);

    	header('Content-type: ' . $file['MyFile']['type']);
    	header('Content-length: ' . $file['MyFile']['size']);
//    	header('Content-Disposition: attachment; filename="'.$file['MyFile']['name'].'"');
    	header('Content-Disposition: inline; filename="'.$file['MyFile']['name'].'"');
    	echo $file['MyFile']['data'];

    	exit();
	}
}

?>
