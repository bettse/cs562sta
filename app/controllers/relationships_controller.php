<?php
class RelationshipsController extends AppController {

	var $name = 'Relationships';
	var $uses = array('Relationship', 'Student', 'Guardian');
	var $helpers = array('Html', 'Form' );
	var $components = array('ObAuth');

	function index() {
		$this->obAuth->lock();
		$this->Relationship->recursive = 0;
		$this->set('relationships', $this->Relationship->findAll());
	}

	function view($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for Relationship.');
			$this->redirect('/relationships/index');
		}
		$this->set('relationship', $this->Relationship->read(null, $id));
	}

	function add() {
		$this->obAuth->lock();
		if(empty($this->data)) {
			$this->set('students', $this->Relationship->Student->generateList());
			$this->set('guardians', $this->Relationship->Guardian->generateList());
			$this->render();
		} else {
			$this->cleanUpFields();
			if($this->Relationship->save($this->data)) {
				$this->Session->setFlash('The Relationship has been saved');
				$this->redirect('/relationships/index');
			} else {
				$this->Session->setFlash('Please correct errors below.');
				$this->set('students', $this->Relationship->Student->generateList());
				$this->set('guardians', $this->Relationship->Guardian->generateList());
			}
		}
	}

	function edit($id = null) {
		$this->obAuth->lock();
		if(empty($this->data)) {
			if(!$id) {
				$this->Session->setFlash('Invalid id for Relationship');
				$this->redirect('/relationships/index');
			}
			$this->data = $this->Relationship->read(null, $id);
			$this->set('students', $this->Relationship->Student->generateList());
			$this->set('guardians', $this->Relationship->Guardian->generateList());
		} else {
			$this->cleanUpFields();
			if($this->Relationship->save($this->data)) {
				$this->Session->setFlash('The Relationship has been saved');
				$this->redirect('/relationships/index');
			} else {
				$this->Session->setFlash('Please correct errors below.');
				$this->set('students', $this->Relationship->Student->generateList());
				$this->set('guardians', $this->Relationship->Guardian->generateList());
			}
		}
	}

	function delete($id = null) {
		$this->obAuth->lock();
		if(!$id) {
			$this->Session->setFlash('Invalid id for Relationship');
			$this->redirect('/relationships/index');
		}
		if($this->Relationship->del($id)) {
			$this->Session->setFlash('The Relationship deleted: id '.$id.'');
			$this->redirect('/relationships/index');
		}
	}

}
?>

