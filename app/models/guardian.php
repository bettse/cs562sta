<?php
class Guardian extends AppModel {

	var $name = 'Guardian';
	var $validate = array(
		'id' => VALID_NUMBER,
		'FirstName' => '/[a-z]$/i',
		'LastName' => '/[a-z]$/i',
		'StreetAddress1' => VALID_NOT_EMPTY,
		'City' => '/[a-z]$/i',
		'State' => '/[a-z]$/i',
		'ZIP' => VALID_NUMBER,
		'Phone1' => VALID_NUMBER,
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
//  var $hasMany = array('Relationship');

    var $hasAndBelongsToMany = array('Student' =>
                                array('className'    => 'Student',
                                     'joinTable'    => 'relationships',
                                     'foreignKey'   => 'guardian_id',
                                     'associationForeignKey'=> 'student_id',
                                     'conditions'   => '',
                                     'order'        => '',
                                     'limit'        => '',
//                                     'unique'       => true,
                                     'finderQuery'  => '',
                                     'deleteQuery'  => '',
                               )
                               );

}
?>
