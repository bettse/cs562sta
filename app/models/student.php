<?php
class Student extends AppModel
{
    var $name = 'Student';
	
	
    var $validate = array(
      'FirstName' => '/[a-z]$/i',
      'LastName' => '/[a-z]$/i',
      'Grade' => VALID_NUMBER,
	  'DOB' => VALID_NOT_EMPTY,
      'start' => '/^[12][0-9]{3}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01]) ([01][0-9]|[2][0-4]):([0-5][0-9]|[0-9]):([0-5][0-9]|[0-9])$/' 
);
   
    var $hasMany = array('Appointment');

    var $hasAndBelongsToMany = array('Guardian' =>
                            array('className'    => 'Guardian',
                                'joinTable'    => 'relationships',
                                'foreignKey'   => 'student_id',
                                'associationForeignKey'=> 'guardian_id',
                                'conditions'   => '',
                                'order'        => '',
                                'limit'        => '',
                                'finderQuery'  => '',
                                'deleteQuery'  => '',
                        )
                        );

}
?>
