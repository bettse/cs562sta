<?php
class Appointment extends AppModel {

	var $name = 'Appointment';
	var $validate = array(
		'id' => VALID_NUMBER,
		'student_id' => VALID_NUMBER,
		'user_id' => VALID_NUMBER,
		'introNotes' => VALID_NOT_EMPTY,
		'finalNotes' => VALID_NOT_EMPTY,
		'score1' => VALID_NUMBER,
		'score2' => VALID_NUMBER,
		'score3' => VALID_NUMBER,
		'score4' => VALID_NUMBER,
		'score5' => VALID_NUMBER,
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
			'Student' =>
				array('className' => 'Student',
						'foreignKey' => 'student_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'counterCache' => ''
				),
			'User' =>
				array('className' => 'User',
						'foreignKey' => 'user_id',
						'conditions' => '',
						'fields' => '',
						'order' => '',
						'counterCache' => ''
				),
	);




//     var $hasMany = array('MyFile');
/*    var $hasOne = array('MyFile' =>
                        array('className'    => 'MyFile',
                              'conditions'   => '',
                              'order'        => '',
//                               'dependent'    =>  true,
                              'foreignKey'   => 'recording1'
                        )
                  );
*/


}
?>